package game;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class Ghost extends Object{
	private boolean UP = false;
	private boolean DOWN = false;
	private boolean LEFT = false;
	private boolean RIGHT = false;
	
	private long lastTime = 0;
	

	int pathLocation = 0;
	ArrayList<Integer> xPath = new ArrayList<Integer>();
	ArrayList<Integer> yPath = new ArrayList<Integer>();
	
	int ghostMode = 0;
		// 0 -> No ghost
		// 1 -> Making Path for Ghost
		// 2 -> Following Ghost Path.
	
	Ghost(float x, float y){
		this.x = x;
		this.y = y;
	}

	public void continuePath(){
		
	}
	
	public void setMode(int mode){
		ghostMode = mode;
		if(mode == 2){
			x = GameEngine.getPlayer().getX();
			y = GameEngine.getPlayer().getY();
		}
	}
	
	public int getMode(){
		return ghostMode;
	}
	
	public void update(Input input){
		double dt = Global.delta;
		switch(getMode()){
			case 0: break;
			case 1: keyMap(input);
					moveGhost(dt);
					saveLocation();
					break;
			case 2: moveGhost(dt);
					break;
		}
		
		x += mx*dt;
		y += my*dt;
	}
	private void saveLocation(){
		if(Global.gameTime-lastTime>100){
			//System.out.println("Recording Ghost Position at:\n\t"+x+", "+y);
			xPath.add((int) x);
			yPath.add((int) y);
			lastTime = Global.gameTime;
		}
	}
	
	private void moveGhost(double dt){
		if(getMode()==1){
			if(!RIGHT && !LEFT){	
				if(mx<-5){
					mx += 800*dt;
				}else if(mx>5){
					mx -= 800*dt;
				}else{
					mx = 0;
				}
			}
			if(!UP && !DOWN){	
				if(my<-5){
					my += 800*dt;
				}else if(my>5){
					my -= 800*dt;
				}else{
					my = 0;
				}
			}
			
			if(RIGHT){
				mx  +=600*dt;
			}
			if(LEFT){
				mx -= 600*dt;
			}
			if(UP){
				my -= 600*dt;
			}
			if(DOWN){
				my += 600*dt;
			}
			if((UP || DOWN) && (LEFT || RIGHT)){
				if(mx>140) mx = 140;
				if(mx<-140) mx = -140;
				if(my>140) my = 140;
				if(my<-140) my = -140;
			}else{
				if(mx>200) mx = 200;
				if(mx<-200) mx = -200;
				if(my>200) my = 200;
				if(my<-200) my = -200;
			}
		
			
			int nextX = (int)(x+mx*dt);
			int nextY = (int)(y+my*dt);
			if(collision(nextX,y)){
				mx = 0;
			}
			if(collision(x,nextY)){
				my = 0;
			}
		}
		else if(getMode() == 2)
		{
			setDirection();
			if( (Math.abs(xPath.get(pathLocation)-x)<3) && (Math.abs(yPath.get(pathLocation)-y)<3)){
				pathLocation++;
				if(pathLocation >= xPath.size()){
					xPath.clear();
					yPath.clear();
					pathLocation = 0;
					setMode(0);
				}
			}
			//System.out.println("Ghost Stats:\n\tPosition is:\n\t"+x+", "+y+"\n\tMomentum is:\n\t"+mx+", "+my+"\n\tPathLocation is: "+pathLocation+"/"+xPath.size());
		}
	}
	
	private void setDirection(){
		double theta = Math.atan2(xPath.get(pathLocation)-x, (yPath.get(pathLocation)-y));
		mx = (float) (150*Math.sin(theta));
		my = (float) (150*Math.cos(theta));
	}
	
	private void keyMap(Input input){
		 UP = false;
		 DOWN = false;
		 LEFT = false;
		 RIGHT = false;

		if(input.isKeyDown(Input.KEY_RIGHT)){
			RIGHT = true;
		}
		
		if(input.isKeyDown(Input.KEY_LEFT)){
			LEFT = true;
		}
		
		if(input.isKeyDown(Input.KEY_UP)){
			UP = true;
		}
		
		if(input.isKeyDown(Input.KEY_DOWN)){
			DOWN = true;
		}
		
	}
	
	private boolean collision(double x, double y){
		if(GameEngine.getLevel().isWall((int)x,(int)y)||
				GameEngine.getLevel().isWall((int)x+10,(int)y) ||
				GameEngine.getLevel().isWall((int)x,(int)y+10) ||
				GameEngine.getLevel().isWall((int)x+10,(int)y+10)
				){
			return true;
		}
		return false;
	}
	
	public void draw(Graphics g){
		switch(getMode()){
			case 0: break;
			case 1: g.setColor(new Color(200,200,200));
					g.fillRect(x, y, 10, 10);
					break;
			case 2: g.setColor(new Color(220,220,220));
					g.fillRect(x, y, 10, 10);
					break;
		}
		
	}
	
	public float getX(){
		return x;
	}
	
	public float getY(){
		return y;
	}
	
	
}
