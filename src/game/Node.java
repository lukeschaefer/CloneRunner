package game;

import java.awt.List;
import java.util.ArrayList;

public class Node extends Object{
	final static int PATROL = 0;
	final static int SEARCH = 1;
	final static boolean VERBOSE = false;
	
	private int nodeMode;
	private int index;
	
	public int getNodeMode() {
		return nodeMode;
	}

	public void setNodeMode(int nodeMode) {
		this.nodeMode = nodeMode;
	}

	public Node(float x, float y, int mode){
		this.x = x;
		this.y = y;
		this.nodeMode = mode;
		this.index = -1;
	}
	
public Node(float x, float y, int mode, int index){
		this.x = x;
		this.y = y;
		this.nodeMode = mode;
		this.index = index;
	}


	public static Node closestNodeTo(Object obj){
		ArrayList<Node> nodes = GameEngine.getNodeList();
		Node closest = null;
		float distance = 10000000;
		for(Node node : nodes){
			float distance2 = node.getDistanceTo(obj);
			if(distance2 < distance){
				distance = distance2;
				closest = node;
			}
		}
		return closest;
	}
	
	public static Node closestNodeTo(Object obj, ArrayList<Node> nodeChoices){
	
		Node closest = null;
		float distance = 10000000;
		for(Node node : nodeChoices){
			float distance2 = node.getDistanceTo(obj);
			if(distance2 < distance){
				distance = distance2;
				closest = node;
			}
		}
		return closest;
	}
	

		
	public static ArrayList<Node> getVisibleNodesFrom(Object obj){		
		ArrayList<Node> visibleNodes = new ArrayList<Node>();
		for(Node node : GameEngine.getNodeList()){
			if(node.hasLOS(obj))
				visibleNodes.add(node);
		}
		if(obj instanceof Node)
			visibleNodes.remove(obj);

		return visibleNodes;
	}
	

		
	public static Node nextNodeTo(Node from, Node to){
		ArrayList<Node> visited = new ArrayList<Node>();
		visited.add(from);
		ArrayList<Node> visible = getVisibleNodesFrom(from);
		
		
		int[][] ranks = new int[visible.size()][2];
		int i = 0;
		if(VERBOSE) System.out.println("Initial pool: "+visible.size());
		if(visible.size() == 1){
			return visible.get(0);
		}
		for(Node nextNode: visible){		
			ranks[i] = nextNode.nextNodeRecursive(to, visited,0);
			i++;
			visited.clear();
			visited.add(from);
		}
		//System.out.println(ranks.length);
		int[] lowest = {1,10000000};
		for(int[] nodeTest : ranks){
			nodeTest[1]+=from.getDistanceTo(GameEngine.getNodeList().get(nodeTest[0]));
			if(VERBOSE) System.out.println("\tNode "+nodeTest[0]+" has a cost of "+nodeTest[1]);			
			if(nodeTest[1] < lowest[1] && from.hasLOS(GameEngine.getNodeList().get(nodeTest[0])))
				lowest = nodeTest;
			
		}
		if(VERBOSE)System.out.println("Best choice is #: "+lowest[0]+" with a count of "+lowest[1]);
		return(GameEngine.getNodeList().get(lowest[0]));
		
		
	}
	private String tabs(int iter){
		String test = "";
		for(int i = 0; i<=iter; i++){
			test += "\t";
		}
		return test;
	}
	private int[] nextNodeRecursive(Node endNode, ArrayList<Node> visited, int iterations){
		iterations++;
		
		if(this.hasLOS(endNode)){
			if(VERBOSE)System.out.println(tabs(iterations)+"Node "+this.index+") Sees end!");
			//Level.getGraphics().drawLine(this.x, this.y, endNode.x, endNode.y);
			return new int[] {this.index,(int) this.getDistanceTo(endNode)};
		}
		ArrayList<Node> visibleNodes = getVisibleNodesFrom(this);	
		visited.add(this);
		visibleNodes.removeAll(visited);
		if(visibleNodes.isEmpty()){
			if(VERBOSE)System.out.println(tabs(iterations)+this.index+") Is a dead end");
			return new int[] {this.index, 99999999};
		}
		int[][] ranks = new int[visibleNodes.size()][2];
		int i = 0;
		if(VERBOSE)System.out.println(tabs(iterations)+this.index+") The "+visibleNodes.size()+" Node choices are: ");
		
		for(Node nextNode: visibleNodes){
			ranks[i] = nextNode.nextNodeRecursive(endNode, visited, iterations);
			i++;
		}
		
		int[] lowest = ranks[0];
		for(int[] nodeTest : ranks){
			nodeTest[1] += (int)getDistanceTo(GameEngine.getNodeList().get(nodeTest[0]));
			//System.out.println(nodesTest[0])
			if(nodeTest[1] < lowest[1])
				lowest = nodeTest;
		}
		//lowest[1]++;
		int cost = (int)getDistanceTo(GameEngine.getNodeList().get(lowest[0]))+lowest[1];
		if(VERBOSE)System.out.println(tabs(iterations)+"Best choice is #: "+lowest[0]+" with a count of "+cost);
		return new int[] {GameEngine.getNodeList().indexOf(this), cost};	
		}
			

}
