package game;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class Button {
	private String text;
	private int width;
	private int height;
	private int x;
	private int y;
	private boolean clicked;
	
		final static int NORMAL = 1;
		final static int HOVER = 2;	
		final static int DOWN = 3;
		final static int CLICK = 4;
	private int mode;
	
	
	public Button(){
		
	}
	
	public Button(String text){
		this.text = text;
		this.clicked = false;
	}

	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean contains(int mouseX, int mouseY) {
		if(mouseX < x + width && mouseX > x && mouseY > y && mouseY < y + height)
			return true;
		return false;
	}

	public void draw(Graphics g) {
		switch(getMode()){
			case HOVER: g.setColor(new Color(30,32,35));
						break;
			case NORMAL:g.setColor(new Color(35,35,38));
						break;
			case DOWN:	g.setColor(new Color(25,25,28));
						break;
			case CLICK:	g.setColor(new Color(255,255,255));
						break;
		}
		
		g.fillRect(x, y, width, height);
		g.setColor(Color.white);
		g.drawString(text, x+width/2-(text.length()*10)/2, y+10);
	}

	private int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;		
	}

	public void update(Input input) {
		setClicked(false);
		int x = input.getMouseX();
		int y = input.getMouseY();
		boolean mouseDown = input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON);
		//boolean mouseClicked = input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON);
	
		
		if(this.contains(x,y))
			
			if(this.contains(x, y) && getMode() == Button.DOWN && !mouseDown){
				setMode(Button.CLICK);
				this.setClicked(true);
			}
			else if(mouseDown)
				this.setMode(Button.DOWN);
			else this.setMode(Button.HOVER);
		else
			this.setMode(Button.NORMAL);
		
	}

	private void setClicked(boolean b) {
		clicked = b;		
	}
	
	public boolean isClicked(){
		return clicked;
	}
}
