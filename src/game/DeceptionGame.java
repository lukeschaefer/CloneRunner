package game;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
 
public class DeceptionGame extends BasicGame{ 
	Menu mainMenu;
	
    public DeceptionGame()
    {
        super("Deception Game");
    }
    
    @Override
    //	LOAD
    public void init(GameContainer gc){
    	gc.setShowFPS(false);
    	mainMenu = new Menu();
    	mainMenu.titleImage("title.png");
    	mainMenu.addButton(new Button("Start Game"));
    	mainMenu.addButton(new Button("Stop Music"));
    	mainMenu.addButton(new Button("Credits"));
    }
 
    @Override
    // UPDATE
    public void update(GameContainer gc, int delta) throws SlickException
	{
    	Global.delta = delta/1000.0;
		Global.gameTime = gc.getTime();
		Input input = gc.getInput();
		if(GameEngine.isLoaded() && !GameEngine.resetting())
			GameEngine.update(input);
		else if (GameEngine.resetting()){
			GameEngine.setLevel(new Level(GameEngine.currentLevel.pathName));
			GameEngine.loadLevel();
		}
		if(mainMenu.isUp()){
			String menuUpdate = mainMenu.update(input);
			if(menuUpdate == "Start Game"){
				Level level1 = new Level("level_1");
				GameEngine.setLevel(level1);
				GameEngine.loadLevel();
				mainMenu.quit();
			}
		}
    }
 
    public void render(GameContainer gc, Graphics g) throws SlickException
	{
    	
    	if(mainMenu.isUp())
    		mainMenu.draw(g);
    	else if(GameEngine.isLoaded() && !GameEngine.resetting())
    		GameEngine.draw(g);
    		
	}
    
    public static void main(String[] args) throws SlickException
    {
         AppGameContainer app =	new AppGameContainer( new DeceptionGame() );
         app.setDisplayMode(800, 600, false);
         app.start();
    }

	public static void nextLevel() {
		String nextLevel = GameEngine.getCurrentLevel().pathName;
		nextLevel = nextLevel.substring(6);
		int nextInt = Integer.parseInt(nextLevel)+1;
		nextLevel = "level_"+nextInt;
		System.out.println("Changing level to "+nextLevel);
		Level next = new Level(nextLevel);
		GameEngine.setLevel(next);
		GameEngine.loadLevel();
		
	}
}




