package game;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;

public class GameEngine {
	static Level currentLevel;
	private static ArrayList<Node>  nodeList = new ArrayList<Node>();
    private static ArrayList<Guard> guardList= new ArrayList<Guard>();
	private static List<Square>	 	walls;
	private static List<Light>		lightList = new ArrayList<Light>();
    private static Player			player;
    private static Music song;
    private static Generator		generator;
    
	private static boolean resetting = false;
    
	public static boolean lightsOn = false;
	private static boolean isLoaded = false;
	
	public static void loadLevel(){
		currentLevel.setLevel(currentLevel.pathName);
		nodeList = currentLevel.getNodes();
		guardList = currentLevel.getGuards();
		walls = currentLevel.getWalls();
		lightList = currentLevel.getLights();
		player = currentLevel.getPlayer();
		generator = currentLevel.getGenerator();
		isLoaded = true;
		resetting = false;
		lightsOn = false;
		try {
			song = new Music("sounds/music/lightsOn.wav");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//music.play();
		song.stop();
		Global.delta = 0;
	}
	
	
	public static void update(Input input){
		player.update(input);
		if(player.getGhost().getMode() != 1)
			Guard.updateGuards(guardList);
		//if(resetting()) return;
		
	}
	
	public static void draw(Graphics g){
		g.drawImage(currentLevel.getBackground(),0,0);
		player.drawPlayer(g);
		player.getGhost().draw(g);
		if(generator != null)
			if(lightsOn){
				System.out.println("Lights On!");			
				Light.drawLights(g, lightList);
		}
			
		
		Guard.drawGuards(g, guardList);
	}

	public static void setMusic(boolean music){
		song.loop();
	}
	
	public static void setLights(boolean on){
		lightsOn = !on;
	}

	public static void setLevel(Level level) {
		currentLevel = level;		
	}
	
	public static Level getLevel() {
		return(currentLevel);		
	}


	public static Level getCurrentLevel() {
		return currentLevel;
	}


	public static void setCurrentLevel(Level currentLevel) {
		GameEngine.currentLevel = currentLevel;
	}


	public static ArrayList<Node> getNodeList() {
		return nodeList;
	}


	public static void setNodeList(ArrayList<Node> nodeList) {
		GameEngine.nodeList = nodeList;
	}


	public static ArrayList<Guard> getGuardList() {
		return guardList;
	}


	public static void setGuardList(ArrayList<Guard> guardList) {
		GameEngine.guardList = guardList;
	}


	public static List<Square> getWalls() {
		return walls;
	}


	public static void setWalls(List<Square> walls) {
		GameEngine.walls = walls;
	}


	public static List<Light> getLightList() {
		return lightList;
	}


	public static void setLightList(List<Light> lightList) {
		GameEngine.lightList = lightList;
	}


	public static Player getPlayer() {
		return player;
	}


	public static void setPlayer(Player player) {
		GameEngine.player = player;
	}


	public static Ghost getGhost() {
		return player.getGhost();
	}


	public static Generator getGenerator() {
		return generator;
	}


	public static void setGenerator(Generator generator) {
		GameEngine.generator = generator;
	}


	public static boolean isLightsOn() {
		return lightsOn;
	}


	public static void setLightsOn(boolean lightsOn) {
		GameEngine.lightsOn = lightsOn;
	}


	public static void playerDied() {
		resetting = true;
		
	}


	public static boolean resetting() {
		return resetting;
	}
	
	public static boolean isLoaded(){
		return isLoaded;
	}


	public static void nextLevel() {
		resetting = true;
		isLoaded = false;
		DeceptionGame.nextLevel();
		
	}
	
	
}
