package game;

public class Global {
	// Options
	public static boolean optionLightPulse = true;
	public static boolean optionLightDynamic = true;
	public static boolean optionPaused = false;
	public static int mouseX;
	public static int mouseY;
	public static double delta;
	public static long gameTime = 0;
	
	// Level Data:
	
	// Functions:
	public static float getDistanceBetween(Object obj1, Object obj2){
		float dx = obj2.getX()-obj1.getX();
		float dy = obj2.getY()-obj1.getY();
		float distance = (float) (Math.sqrt((dx*dx)+(dy*dy))/2);
		return distance;
	}
	
	public static float getAngleDifference(float angle1, float angle2){
		return Math.abs((angle1 + 180 -  angle2) % 360 - 180);
	}

}
