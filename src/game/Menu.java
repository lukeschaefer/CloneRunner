package game;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Menu {
	ArrayList<Button> buttons = new ArrayList<Button>();
	private boolean active = false;
	Image titleImage;
	Image background;
	boolean running = true;
	
	public Menu(){
		active = true;
		try {
			background = new Image("images/background.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void draw(Graphics g){
		g.setColor(new Color(20,22,24));
		g.drawImage(background,0,0);
		for(Button button : buttons){
			button.draw(g);
		}
		if(titleImage != null){
			g.drawImage(titleImage, 400-titleImage.getWidth()/2, 50);
		}
	}
	
	public void addButton(Button button){
		button.setX(300);
		button.setWidth(200);
		button.setY(200+(buttons.size()*40));
		button.setHeight(40);
		button.setMode(Button.NORMAL);
		buttons.add(button);
		
	}

	private void clicked(Button button){
		System.out.println("The button "+button.getText()+" was clicked.");
	}
	
	
	public boolean isUp() {
		return running;
	}

	public String update(Input input) {
		for(Button button : buttons){
			button.update(input);
			if(button.isClicked()){
				clicked(button);
				return(button.getText());
			}
		}
		return "";
		
		
	}

	public void titleImage(String string) {
		try {
			titleImage = new Image("images/"+string);
		} catch (SlickException e) {
			System.err.println("Error loading title image "+string);
			e.printStackTrace();
		}
		for(Button button : buttons)
			button.setY(button.getY()+titleImage.getHeight());
		
	}

	public void quit() {
		running = false;
		
	}
}
