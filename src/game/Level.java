package game;

import org.newdawn.slick.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;


public class Level {
	Image levelSpace;
	int blockSize;
	File mapName;
	String pathName;
	String levelName;
	
	// Level Maps:
	Image 			levelImage;
	BufferedImage 	image;
	BufferedImage 	collisionMap;
	BufferedImage	lineOfSightMap;
	
	// Sounds:
	Music music1;
	Music music2;
	
	// Object Lists:
	private  ArrayList<Node> nodeList = new ArrayList<Node>();
    private ArrayList<Guard> guardList= new ArrayList<Guard>();
	private List<Square> walls;
	private List<Light> lightList = new ArrayList<Light>();
    private Player player;
    private Ghost ghost;
    private Generator generator;
    private ArrayList<Square> endZone = new ArrayList<Square>();
   
    private Graphics graphics;
    
    private boolean lightsOn = true;
    
    private boolean resetting = false;
    
    static boolean active = false;
    
	public Level(String levelString){
		pathName = levelString;
	}
	
	public void setLevel(String levelName){
		pathName = levelName.toLowerCase();
		mapName = new File(pathName+"/map.png");
		try {
			image = ImageIO.read(mapName);
			levelImage = new Image(pathName+"/mapImage.png");
			collisionMap = ImageIO.read(new File(pathName+"/mapCollision.png"));
			lineOfSightMap = ImageIO.read(new File(pathName+"/mapLOS.png"));
			walls = Square.createLevelSquares(levelName);
			System.out.println("LEVEL \""+pathName+"\" LOADED");
		} catch (IOException e) {
			System.out.println("ERROR GENERATING LEVEL:\n\t"+mapName);
			
			//e.printStackTrace();
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("GETTING BLOCK COORDINATES");
		try {
			Light.init();
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getObjects();
	}
	
	
	
	
	private static boolean isColor(int clr, String color2){
		int r=0;
		int g=0;
		int b=0;
		if(color2 == "yellow"){
			r=250;
			g=250;
			b=0;
		}
		if(color2 == "red"){
			r=250;
			g=0;
			b=0;
		}
		if(color2 == "blue"){
			r=0;
			g=0;
			b=250;
		}
		if(color2 == "white"){
			r=250;
			g=250;
			b=250;
		}
		if(color2 == "green"){
			r=0;
			g=250;
			b=0;
		}
		if(color2 == "grey" || color2 == "gray"){
			r=126;
			g=126;
			b=126;
		}
		if(color2 == "pink"){
			r=250;
			g=0;
			b=250;
		}
		if(color2 == "cyan"){
			r=0;
			g=250;
			b=250;
		}
		int  red   = (clr & 0x00ff0000) >> 16;
		int  green = (clr & 0x0000ff00) >> 8;
		int  blue  =  clr & 0x000000ff;
		if(Math.abs(red-r)<10 && Math.abs(green-g)<10 && Math.abs(blue-b)<10){
			return true;
		}
		
		return false;
	}
	
	private void getObjects(){
		blockSize = 800/image.getWidth();
		

		for(int y = 0; y < image.getHeight(); y++){
			for(int x = 0; x < image.getWidth();x++){
				 int clr=  image.getRGB(x,y); 
				 if(isColor(clr, "green")){
					 System.out.println("Adding player at "+(x*blockSize)+", "+(y*blockSize));
					 addPlayer(x*blockSize,y*blockSize);
				}
				if(isColor(clr, "red")){
					 System.out.println("Adding guard at "+(x*blockSize)+", "+(y*blockSize));
					 addGuard(x*blockSize,y*blockSize);
				}
				if(isColor(clr, "blue")){
					 System.out.println("Adding PATROL node at "+(x*blockSize)+", "+(y*blockSize));
					 addNode(x*blockSize,y*blockSize, Node.PATROL);
				}
				if(isColor(clr, "pink")){
					 System.out.println("Adding SEARCH node at "+(x*blockSize)+", "+(y*blockSize));
					 addNode(x*blockSize,y*blockSize, Node.SEARCH);
				}
				if(isColor(clr, "yellow")){
					 System.out.println("Adding Light at "+(x*blockSize)+", "+(y*blockSize));
					 addLight(x*blockSize,y*blockSize);
				}
				if(isColor(clr, "grey")){
					 System.out.println("Adding Generator at "+(x*blockSize)+", "+(y*blockSize));
					 generator = new Generator(x*blockSize, y*blockSize);
				}
				if(isColor(clr, "cyan")){
					System.out.println("Adding endblock at "+(x*blockSize)+", "+(y*blockSize));
					addEndZone(x*blockSize, y*blockSize);
				}
				
			}
		}
	
		if(generator != null){
			lightsOn = true;
			Light.getWallShadows(lightList, walls);
			}
	}
	
	private void addEndZone(int x, int y){
		endZone.add(new Square(x,y,blockSize, blockSize));
	}
	
	public boolean inEndZone(int x, int y){
		for(Square square : endZone){
			if(square.getBox().contains(x, y)){
				return true;
			}
		}
		return false;
	}
	
	
	public Player getPlayer(){
		return player;
	}
	
	public boolean isWall(int x, int y){
		if(x<0 || y<0 || x>800 || y > 600) return true;
		int clr = collisionMap.getRGB(x, y);
		 int  red   = (clr & 0x00ff0000) >> 16;
		 int  green = (clr & 0x0000ff00) >> 8;
		 int  blue  =  clr & 0x000000ff;
		 if( red == 255 && green == 255 && blue == 255)
			return true;
		 return false;	
	}
	
	public boolean isBlockingSight(int x, int y){
		int clr = lineOfSightMap.getRGB(x, y);
		 int  red   = (clr & 0x00ff0000) >> 16;
		 int  green = (clr & 0x0000ff00) >> 8;
		 int  blue  =  clr & 0x000000ff;
		 if( red == 255 && green == 255 && blue == 255)
			return true;
		 return false;	
	}
	
	public BufferedImage getImage(){
		return image;
	}
	
	public void addPlayer(float x, float y){
		player = new Player(x,y);		
	}
	
	public void addGuard(float x, float y){
		Guard newGuard = new Guard(x,y);
		guardList.add(newGuard);
	}
	
	public void addLight(float x, float y){
		Light newLight = new Light(x,y);
		lightList.add(newLight);
	}
	
	
	public void addNode(float x, float y, int z){
		Node newNode = new Node(x,y,z,nodeList.size());
		nodeList.add(newNode);
	}
	
	public ArrayList<Node> getNodes(){
		return nodeList;
	}
	
	public void updateObjects(Input input){
		if(ghost.getMode() == 0){
			if(player != null){
				if(input.isKeyDown(Input.KEY_SPACE)){
					createGhost();
				}
				player.update(input);
			}
			Guard.updateGuards(guardList);
			if(resetting()) return;
		}else if(ghost.getMode() == 1){
			ghost.update(input);
		}else{
			ghost.update(input);
			if(player != null){
				//if(input.isKeyDown(Input.KEY_SPACE)){
				//	createGhost();
				//}
				player.update(input);
				Guard.updateGuards(guardList);
			}
		}
	}
	public void createGhost(){
		ghost = new Ghost(player.getX(), player.getY());
		ghost.setMode(1);
		
	}
	
	public void clearLists(){
		guardList.clear();
		lightList.clear();
		lightsOn = true;
		resetting = false;
	}
	public boolean lightsOn(){
		return lightsOn;
	}
	
	public Generator getGenerator(){
		return generator;
	}
	public Ghost getGhost(){
		return ghost;
	}
	
	public void drawObjects(Graphics g){
		if(graphics == null)
			graphics=g;
		g.drawImage(levelImage,0,0);
		
		
		if(player != null) player.drawPlayer(g);
		ghost.draw(g);
		if(generator != null){
			//generator.draw(g);
			if(lightsOn){
				//g.setColor(new Color(0,5,10,200));
				//g.fillRect(0, 0, 800, 600);
				Light.drawLights(g, lightList);
				
			}
		}
		Guard.drawGuards(g, guardList);
	}
	
	public ArrayList<Guard> getGuards(){
		return guardList;
	}

	public List<Square> getWalls() {
		return walls;
	}

	public void setLights(boolean b) {
		lightsOn = b;		
	}

	public List<Light> getLights() {
		return lightList;
	}

	public void setMusic(boolean mode) {
		if(mode){
			music2.stop();
			music1.loop();
		}else{
			music1.stop();
			music2.loop();	
		}
	}

	public boolean resetting() {
		return resetting;
	}
	public void reset(){
		resetting = true;
	}

	public boolean inGame() {
		return active;
	}

	public Image getBackground() {
		return levelImage;
	}
	
}