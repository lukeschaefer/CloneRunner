package game;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;


public class Guard extends Object{
	boolean LOS = false;
	boolean seen = false;
	boolean notSet = true;
	
	Color guardColor = new Color (255,0,0);
	
	int mode = 0;
	//	0 = idle
	//	1 = chasing target
	//	2 = lost sight of target
	//	3 = Alert mode?
	Image image;
	float imageRotate = 0;
	int lostCount = 0;
	
	float lastSeenX;
	float lastSeenY;
	float lastSeenDirection;
	Object targetNode;
	
	Random random = new Random();
	
	final static int WALK = 70;
	final static int RUN = 150;
	final static int SPRINT = 200;
	
	final static int MODE_IDLE = 0;
	final static int MODE_CHASE = 1;
	final static int MODE_LOST = 2;
	final static int MODE_GENERATOR = 3;
	
	private int currentSpeed = WALK;
	
	Object lastNode;
	Object target;
	//private Object lostTarget;
	
	Guard(float x, float y){
		this.x = x;
		this.y = y;
		try {
			image = new Image("images/guard.png");
		} catch (SlickException e) {
			System.out.println("Error getting guard image.");
			e.printStackTrace();
		}
		setMode(Guard.MODE_IDLE);
	}
	
	public static void drawGuards(Graphics g, ArrayList<Guard> guardList){
		for(Guard guard : guardList){
			guard.drawGuard(g);
		}
		
	}
	
	public static void updateGuards(ArrayList<Guard> guardList){
		for(Guard guard : guardList){
			if(GameEngine.resetting()){
				break;
			}
			switch(guard.getMode()){
			case 0: guard.idleUpdate();
					break;
			case 1: guard.sightUpdate();
					break;
			case 2: guard.lostUpdate();
					break;
			case 3: guard.generatorUpdate();
					guard.idleUpdate();
					break;
			}
		
			
			
			guard.setDirection(guard.getTarget(), guard.getSpeed());
			if(guard.getDistanceTo(GameEngine.getPlayer())<10){
				GameEngine.playerDied();
			}
			if(guard.getTarget() != null)
				if(guard.atTarget()){
					guard.targetUpdate();
				}
			for(Guard otherGuard : guardList){
				if(!otherGuard.equals(guard)){
					guard.moveAwayFrom(otherGuard);
				}
			}
			guard.collisionDetect();
			if(guard.mx == 0 && guard.my != 0)
				if(guard.my>0)
					guard.my = 70;
				else
					guard.my = -70;
			
			if(guard.my == 0 && guard.mx != 0)
				if(guard.mx>0)
					guard.mx = 70;
				else
					guard.mx = -70;
			if(guard.my == 0 && guard.mx == 0){
				guard.targetUpdate();
			}
			guard.x += guard.mx*Global.delta;
			guard.y += guard.my*Global.delta;
				
		}		
	}
	
	private void targetUpdate() {
		if(getMode() == Guard.MODE_IDLE){
			this.setTarget(this.nextNode());
		}
		if(getMode() == Guard.MODE_LOST){
			this.setMode(Guard.MODE_IDLE);
		}
		if(getMode() == Guard.MODE_GENERATOR){
			//System.out.println(null);			
			this.setTarget(Node.nextNodeTo(Node.closestNodeTo(this), new Node(GameEngine.getGenerator().getX(), GameEngine.getGenerator().getY(), Node.PATROL)));
			if(this.hasLOS(GameEngine.getGenerator()))
				this.setTarget(GameEngine.getGenerator());
			else
				if(this.getDistanceTo(this.getTarget())<10)
					this.setMode(MODE_IDLE);
				
			//System.out.println("Success");
		}
		
	}
	
	private Node nextNode(){
	
		ArrayList<Node> options = nodeChoices(Node.PATROL);
		if(options.isEmpty())
			options = nodeChoices(Node.SEARCH);
		
		ArrayList<Node> goodOptions = new ArrayList<Node>();
		for(Node node : options){
			if(Math.abs(this.getAngleTo(node)%90)<2)			
				goodOptions.add(node);
		}
		if(!goodOptions.isEmpty())
			return(goodOptions.get(random.nextInt(goodOptions.size())));
		if(!options.isEmpty())
			return(options.get(random.nextInt(options.size())));
		return null;
			
	}

	private int getSpeed() {
		return currentSpeed;
	}
	
	private void setSpeed(int speed){
		currentSpeed = speed;
	}

	private boolean atTarget(){
		if(this.getDistanceTo(this.getTarget())<3)
			return true;
		return false;
	}
	private void generatorUpdate(){
		setSpeed(Guard.RUN);
		if(this.getDistanceTo(GameEngine.getGenerator())<10){
			GameEngine.getGenerator().setMode(true);
			for(Guard guard : GameEngine.getGuardList()){
				if(guard.getMode() == MODE_GENERATOR){
					guard.setMode(MODE_IDLE);
				}
			}
		}
	}
	private void idleUpdate(){
		setSpeed(Guard.WALK);
		Object player = GameEngine.getPlayer();
		Object ghost  = GameEngine.getGhost();		
		if(target == null && getMode() == MODE_IDLE)
			target = nextNode();
		if(this.sees(player) && this.sees(ghost) && this.isFacing(player) && this.isFacing(ghost)){
			mode = 1;
			//Go after what less are going after:
			int ghostTarget = 0;
			int playerTarget = 0;
			for(Guard guard : GameEngine.getGuardList()){
				if(guard.getTarget() instanceof Player) playerTarget++;
				if(guard.getTarget() instanceof Ghost) ghostTarget++;
			}
			//target = ghost;
			ghostTarget++;
			if(ghostTarget>playerTarget){
				setTarget(player);
			}
			
			//target = Level.getGhost();
			
		}else if(this.sees(player) && this.isFacing(player)){
			setTarget(player);
			mode = 1;
		}else if(this.sees(ghost) && this.isFacing(ghost)){
			target =  ghost;
			mode = 1;
		}
	}
	
	private void sightUpdate(){
		setSpeed(Guard.RUN);
		guardColor = new Color(250,00,00);
		LOS = true;
		
		if(!this.sees(target)){
			mode = 2;
			notSet = true;
			//lostTarget = target;
		}
		
		
		Object player = GameEngine.getPlayer();
		Object ghost  = GameEngine.getGhost();		
		
	
		
		
		if(this.sees(player) && this.sees(ghost) && notSet){
			mode = 1;
			//Go after what less are going after:
			int ghostTarget = 0;
			int playerTarget = 0;
			for(Guard guard : GameEngine.getGuardList()){
				if(guard.getTarget() instanceof Player) playerTarget++;
				if(guard.getTarget() instanceof Ghost) ghostTarget++;
			}
			if(ghostTarget > playerTarget){
				target = player;
			}else target = ghost;
			notSet = false;
		}
			
			
		setDirection(target, RUN);
		//lastSeenDirection = target.getDirection();
	}
	
	public ArrayList<Node> nodeChoices(int search){
		ArrayList<Node> nodes = new ArrayList<Node>();
		for(Node node : GameEngine.getNodeList()){
			if(node.getNodeMode() == search && this.sees(node)){
				nodes.add(node);				
			}
		}
		return  nodes;
	}
		
	private void lostUpdate(){
		setSpeed(Guard.RUN);
		if(LOS){
			lostCount = 0;
			ArrayList<Node> nodeChoices = Node.getVisibleNodesFrom(this);
			setTarget(Node.closestNodeTo(target, nodeChoices));			
			LOS = false;
			System.out.println("LOST INIT");
		}

		
		Object player = GameEngine.getPlayer();
		Object ghost = GameEngine.getGhost();
		
		if(this.sees(player) && this.sees(ghost)){
			target = GameEngine.getGhost();
			mode = 1;
			if(Global.getDistanceBetween(this, player) > Global.getDistanceBetween(this, ghost)){
				target = player;
			
			}
			
		}else if(this.sees(player)){
			target = player;
			mode = 1;
		}else if(this.sees(ghost)){
			target =  ghost;
			mode = 1;
		}
		
	}
	
	
	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public Object getTarget() {
		return target;
	}

	public void setTarget(Object target) {
		this.target = target;
	}

	public boolean isFacing(Object obj){
		float dx = x-obj.getX();
		float dy = y-obj.getY();
		if(!(obj instanceof Node))
			if(Global.getAngleDifference((float)Math.toDegrees(Math.atan2(dy,dx)), (float) Math.toDegrees(theta))<90){
				return false;
			}
		return true;
	}
	
	public boolean sees(Object obj){
		if(obj instanceof Ghost){
			if (((Ghost) obj).getMode() != 2){
				return false;
			}
		}
		if(this.hasLOS(obj)){
			if(obj instanceof Player){
				if(((Player) obj).getBrightness()*5 < getDistanceTo(obj)){
					return false;
				}
			}
		}else{
			return false;
		}
		return true;
	}
	
	
	public void drawGuard(Graphics g){
		g.setColor(guardColor);
		this.image.setRotation((float) Math.toDegrees(theta)-90);
		g.drawImage(this.image, x, y);
		if(getTarget() != null){
			g.setColor(Color.white);
			//g.drawString(""+getMode(), getX(), getY()-10);
			
			//g.drawLine(getX(), getY(), getTarget().getX(), getTarget().getY());
		}
			
	}
	
	public void setTarget(Object obj, int speed){
		target = obj;
		//theta = (float) (Math.atan2((x-obj.getX()), (y-obj.getY()))-Math.PI);
	}
	
	public void setDirection(Object obj, int speed){
		if(obj != null)
			theta = (float) (Math.atan2(this.y-obj.getY(),this.x - obj.getX())-Math.PI);
		else
			speed = 0;
		mx = (float) (speed*Math.cos(theta));
		my = (float) (speed*Math.sin(theta));
	}
	
	private void moveAwayFrom(Guard otherGuard){
		float distance = Global.getDistanceBetween(this, otherGuard);	
		if(distance < 20){
			double repelForce = (1.0/distance)*300;
			double theta = Math.atan2((x-otherGuard.getX()), (y-otherGuard.getY()));
			float repelX = (float) (repelForce*Math.sin(theta));
			float repelY = (float) (repelForce*Math.cos(theta));
			mx += repelX;
			my += repelY;
		}
	}
		
		
	public static void findGenerator(){
		//for(Guard guard : GameEngine.getGuardList()){
			//if(5==(int)(Math.random()*10))
					//guard.setMode(MODE_GENERATOR);
			
		//}
	}
	
	public boolean seesTargets(){
		Object player = GameEngine.getPlayer();
		Object ghost  = GameEngine.getGhost();
		if(this.sees(player) || this.sees(ghost)){
			return true;
			}
		return false;
	}
	
	
	

	
	
}
