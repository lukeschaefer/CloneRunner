package game;
public class Object {
	float x;
	float y;
	float mx;
	float my;
	float theta;
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getMx() {
		return mx;
	}

	public void setMx(float mx) {
		this.mx = mx;
	}

	public float getMy() {
		return my;
	}

	public void setMy(float my) {
		this.my = my;
	}

	public static float getDistanceTo(Object obj1, Object obj2){
		float dx = obj2.getX()-obj1.getX();
		float dy = obj2.getY()-obj1.getY();
		float distance = (float) (Math.sqrt((dx*dx)+(dy*dy))/2);
		return distance;
	}

	public float getDistanceTo(Object obj2){
		float dx = obj2.getX()-this.getX();
		float dy = obj2.getY()-this.getY();
		float distance = (float) (Math.sqrt((dx*dx)+(dy*dy))/2);
		return distance;
	}
	
	public boolean hasLOS(Object obj2){
		float dx = x-obj2.getX();
		float dy = y-obj2.getY();
		float distance = getDistanceTo(obj2);
		for(int i = 0; i<distance; i+=2){	
			int testX = (int)(obj2.getX() + (dx/distance)*i);
			int testY = (int)(obj2.getY() + (dy/distance)*i);
			if(GameEngine.getLevel().isBlockingSight(testX, testY)){
				return false;
			}
		}
		return true;
	}
	
	Object(){}
	
	Object(float x, float y){
		setX(x);
		setY(y);
	}
	
	protected void collisionDetect(){
		float x = getX();
		float y = getY();
		float[][] collisionPoints = 	{
									{x,y},
									{x+5,y},
									{x+10,y},
									{x,y+5},
									{x+10,y+5},
									{x,y+10},
									{x+5,y+10},
									{x+10,y+10}
									};
		
		
		
		
		for(float[] point : collisionPoints){	
			int nextY = (int) (point[1]+this.my*Global.delta);
			if(GameEngine.getLevel().isWall((int) point[0], nextY)){
				my = 0;
				break;
			}
			
		}
		for(float[] point : collisionPoints){	
			int nextX = (int) (point[0]+this.mx*Global.delta);
			if(GameEngine.getLevel().isWall(nextX, (int) point[1])){
				mx = 0;
				break;
			}
		}
			
	}
	
	protected float getAngleTo(Object obj){
		float dx = x-obj.getX();
		float dy = y-obj.getY();
		return (float) Math.toDegrees(Math.atan2(dy,dx));
	
	}
	
	
		
}
