package game;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;


public class Player extends Object{
	
	private boolean UP = false;
	private boolean DOWN = false;
	private boolean LEFT = false;
	private boolean RIGHT = false;
	private boolean ACTION = false;
	
	
	
	int rotation;
	private Image image;
	Ghost ghost;
		
	boolean ground;
	public Player(int x, int y){
		ghost = new Ghost(x,y);
		this.x = x;
		this.y = y;
	}
	
	public Player(float x2, float y2) {
		ghost = new Ghost(x,y);
		this.x = x2;
		this.y = y2;
		rotation = 0;
		try {
			image = new Image("images/player.png");
		} catch (SlickException e) {
			System.out.println("Error getting player image");
			e.printStackTrace();
		}
	}

	public void update(Input input){
		
		double dt = Global.delta;
		keyMap(input);
		if(!input.isKeyDown(Input.KEY_SPACE)){
			if(input.isKeyDown(Input.KEY_K))
				GameEngine.nextLevel();
			if(ghost.getMode() == 1){
				ghost.setMode(2);
			}
				
			if(!RIGHT && !LEFT){	
				if(mx<-5){
					mx += 800*dt;
				}else if(mx>5){
					mx -= 800*dt;
				}else{
					mx = 0;
				}
			}
			if(!UP && !DOWN){	
				if(my<-5){
					my += 800*dt;
				}else if(my>5){
					my -= 800*dt;
				}else{
					my = 0;
				}
			}
			
			if(RIGHT){
				mx  +=600*dt;
			}
			if(LEFT){
				mx -= 600*dt;
			}
			if(UP){
				my -= 600*dt;
			}
			if(DOWN){
				my += 600*dt;
			}
			if((UP || DOWN) && (LEFT || RIGHT)){
				if(mx>140) mx = 140;
				if(mx<-140) mx = -140;
				if(my>140) my = 140;
				if(my<-140) my = -140;
			}else{
				if(mx>200) mx = 200;
				if(mx<-200) mx = -200;
				if(my>200) my = 200;
				if(my<-200) my = -200;
			}
		}
		else if(ghost.getMode() == 0){
			createGhost();
		}
		if(ghost.getMode() == 1){
			mx = 0;
			my = 0;
		}
	
		ghost.update(input);
		
		
		collisionDetect();
		x += mx*dt;
		y += my*dt;
		if(ACTION)
			if(this.getDistanceTo((Object)GameEngine.getGenerator()) < 10){
				GameEngine.getGenerator().setMode(false);
			}
		if(GameEngine.getLevel().inEndZone((int)this.x,(int) this.y)){
			for(Guard guard : GameEngine.getGuardList()){
				if(guard.mode == Guard.MODE_CHASE || guard.mode == Guard.MODE_LOST){
					System.out.println("Can't finish, some guards not lost...");				
					return;
				}
				
			}
			GameEngine.nextLevel();
		}
	}
	
	private void createGhost(){
		ghost = new Ghost(this.getX(), this.getY());
		ghost.setMode(1);
		
	}
	
	public void drawPlayer(Graphics g){
		g.setColor(new Color(255,255,255));
		
		if(UP && !DOWN && !RIGHT && !LEFT)
			rotation = 0; else
		if(UP && !DOWN && RIGHT && !LEFT)
			rotation = 45; else
		if(!UP && !DOWN && RIGHT && !LEFT)
			rotation = 90; else
		if(!UP && DOWN && RIGHT && !LEFT)
			rotation = 135; else
		if(!UP && DOWN && !RIGHT && !LEFT)
			rotation = 180; else
		if(!UP && !DOWN && !RIGHT && LEFT)
			rotation = 270; else
		if(!UP && DOWN && !RIGHT && LEFT)
			rotation = 225; else
		if(UP && !DOWN && !RIGHT && LEFT)
			rotation = 315; 
		image.setRotation((float)rotation+180);
		g.drawImage(image, x, y);
		//g.drawString(""+getBrightness(), x, y-10);
	}
	
	private void keyMap(Input input){
		 UP = false;
		 DOWN = false;
		 LEFT = false;
		 RIGHT = false;
		 ACTION = false;
		 
		if(input.isKeyDown(Input.KEY_RIGHT)){
			RIGHT = true;
		}
		
		if(input.isKeyDown(Input.KEY_LEFT)){
			LEFT = true;
		}
		
		if(input.isKeyDown(Input.KEY_UP)){
			UP = true;
		}
		
		if(input.isKeyDown(Input.KEY_DOWN)){
			DOWN = true;
		}
		if(input.isKeyDown(Input.KEY_E)){
			ACTION = true;
		}
		
	}
	
	
	
	public boolean isMoving(){
		if(UP || DOWN || LEFT || RIGHT){
			return true;
		}
		return false;
	}
	
	public int getBrightness(){
		if(isMoving()){
			return (int)(Light.getLightAmountFor(this)*1.5);
		}
		return Light.getLightAmountFor(this);
	}

	public Ghost getGhost() {
		return ghost;
	}
}
