package game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Polygon;


public class Light extends Object{
	


	private Color color;
	private static Image lightBasic;
	
	private static Image fullLightImage;
	private static Graphics fullLightImageG;
	
	private static Image lightsOn;
	
    private Image lightImage;
    private Graphics lightImageG;
    
    private Image shadowsImage;
    
    
    
    
	
	Light(){	
	}
	
	public static void init() throws SlickException{
		System.out.println("Level path: "+GameEngine.getLevel().pathName);
		lightBasic = new Image("/images/light.png");		
		lightsOn = new Image(GameEngine.getLevel().pathName+"/mapLightsOn.png");
		fullLightImage = new Image(800,600);
		fullLightImageG = fullLightImage.getGraphics();
	}
	
	Light(float x, float y){
		this.x = x;
		this.y = y;   		
		this.color = new Color(255,255,200);
    	try {

    		lightImage = new Image(600,600);
    		lightImageG = lightImage.getGraphics();
    	} catch (SlickException e) {
		System.out.println("creating local image or graphics context failed: " + e.getMessage());
    	}		

	}
	
	

	public void draw(List<Square> squares){	
		
		lightImageG.setBackground(new Color(0,0,0,0));		
		lightImageG.clear();
		//lightImageG.setBackground(new Color(0,0,0,0));
		
		//GL11.glColorMask(true, true, true, false);	
		lightImageG.setDrawMode(Graphics.MODE_NORMAL);	
		//lightImageG.setDrawMode(Graphics.MODE_ADD);
		lightImageG.drawImage(lightBasic, 0 ,0);
		
					
		lightImageG.drawImage(shadowsImage,0,0);
		lightImageG.setColor(Color.black);
		
		for(Guard guard : GameEngine.getGuardList()){
			Square guardShadow = new Square((int)guard.getX()+3, (int)guard.getY()+3, 8,8);
			Polygon[] shadows = (guardShadow.getShadows(getX(), getY()));
			for(Polygon shadow: shadows){
				lightImageG.fill(shadow);
				
			}
		}
		lightImageG.setDrawMode(Graphics.MODE_COLOR_MULTIPLY);
		GL11.glBlendFunc(GL11.GL_DST_COLOR, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_BLEND);
		lightImageG.setColor(this.color);
		lightImageG.fillRect(0, 0, 600, 600);
		
		lightImageG.flush();
		
		GL11.glColorMask(true, true, true, true);	
		fullLightImageG.setDrawMode(Graphics.MODE_ADD);
		fullLightImageG.drawImage(lightImage, this.getX()-300, this.getY()-300);
		
		fullLightImageG.setDrawMode(Graphics.MODE_NORMAL);
		//g.setColor(Color.yellow);
		//g.fillRect(this.getX()-2, this.getY()-2,4,4);
	}
	
	public static void drawLights(Graphics g, List<Light> lightList) {
		
		fullLightImageG.clear();
		fullLightImageG.setBackground(new Color(0,0,0));
		fullLightImageG.setColor(new Color(30,30,32));
		fullLightImageG.fillRect(0, 0, 800, 600);
		
		for(Light drawLight : lightList){
			drawLight.draw(GameEngine.getWalls());
		}
		g.setDrawMode(Graphics.MODE_NORMAL);
		g.setDrawMode(Graphics.MODE_COLOR_MULTIPLY);
		GL11.glBlendFunc(GL11.GL_DST_COLOR, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_BLEND);
		fullLightImageG.flush();	
	
		g.drawImage(fullLightImage,0,0);
		g.setDrawMode(Graphics.MODE_NORMAL);
		g.drawImage(lightsOn,0,0);
	}

	public static void getWallShadows(List<Light> lightList, List<Square> walls) {
		for(Light light : lightList){
			light.getShadowList(walls);
		}
		
	}

	private void getShadowList(List<Square> walls) {
		Graphics shadowGraphics;
		try {
			shadowsImage = new Image(600,600);
			shadowGraphics = shadowsImage.getGraphics();
			shadowGraphics.setBackground(Color.white);
			shadowGraphics.setColor(Color.black);
			shadowGraphics.setAntiAlias(true);
			for(Square thisSquare : walls){
				Polygon[] shadows = thisSquare.getShadows(getX(), getY());
				for(int i = 0; i<4; i++)
					shadowGraphics.fill(shadows[i]);
				shadowGraphics.flush();
			}
		} catch (SlickException e) {
			e.printStackTrace();
		}
		
		
		
	}

	public static int getLightAmountFor(Object obj) {
		int totalLight = 5;
		if(GameEngine.isLightsOn()){
			return 1000;
		}else{
			for(Light light : GameEngine.getLightList()){
				if(light.hasLOS(obj))
					totalLight += Math.max(600-light.getDistanceTo(obj),0)/30;
			}
		}
		return totalLight;
	}
}
