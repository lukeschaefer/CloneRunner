package game;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;
class Square {
	private static List<Square> squares= new ArrayList<Square>();
	
	int vertices[] = new int[8];
	double[] color = {Math.random(),Math.random(), Math.random()};
	int size;
	float xPos;
	float yPos;
	
	Rectangle box;
	Polygon shadow;
	Square(){
	
	}

	Square(int x, int y)
	{
		size = 50;
		xPos = x;
		yPos = y;
		vertices[0]  =  x;
		vertices[1]  =  y;
		vertices[2]  =  x+size;
		vertices[3]  =  y;
		vertices[4]  =  x+size;
		vertices[5]  =  y+size;
		vertices[6]  =  x;
		vertices[7]  =  y+size;
		box = new Rectangle(xPos ,yPos, size+1, size+1);
	}
	
	Square(int x, int y, int width, int height)
	{
		xPos = x;
		yPos = y;
		vertices[0]  =  x;
		vertices[1]  =  y;
		vertices[2]  =  x+width;
		vertices[3]  =  y;
		vertices[4]  =  x+width;
		vertices[5]  =  y+height;
		vertices[6]  =  x;
		vertices[7]  =  y+height;
		box = new Rectangle(xPos ,yPos, width+1, height+1);
	}
	
	public void setPos(int x, int y){
		xPos = x;
		yPos = y;
		vertices[0]  =  x;
		vertices[1]  =  y;
		vertices[2]  =  x+size;
		vertices[3]  =  y;
		vertices[4]  =  x+size;
		vertices[5]  =  y+size;
		vertices[6]  =  x;
		vertices[7]  =  y+size;
		box = new Rectangle(xPos ,yPos, size+1, size+1);
	}

	public void draw(){
		
		GL11.glColor3d(color[0], color[1], color[2]);
		
		GL11.glLineWidth(2);
		GL11.glBegin(GL11.GL_LINES);
		for(int i = 0; i <4; i++){			
			GL11.glVertex2f(getWall(i)[0],getWall(i)[1]);
			GL11.glVertex2f(getWall(i)[2],getWall(i)[3]);
		}
		GL11.glEnd();
		
		
		
		
		GL11.glColor3d(color[0]-.4, color[1]-.4, color[2]-.4);
		GL11.glBegin(GL11.GL_QUADS);

		for(int i = 0; i <4; i++){
			
			GL11.glVertex2f(getWall(i)[0],getWall(i)[1]);

			
		}
		GL11.glEnd();

	}
	
	public Polygon[] makeShadow(float f, float g){
		GL11.glColor3d(.1, .1, .1);
		
		Polygon shadows[] = new Polygon[4];
		for(int i = 0; i<4; i++)
		{
			int wall[] = getWall(i);
			float[] shadowVerts = getShadow(wall, f, g);
			shadow = new Polygon(shadowVerts);
			shadows[i] = shadow;
		}
		return shadows;
	}
	
	private int[] getWall(int i){
		i *= 2;
		int wall[] = new int[4];
		
		wall[0] = vertices[i];
		wall[1] = vertices[i+1];
		
		if(i!=6)
			wall[2] = vertices[i+2];
		else
			wall[2] = vertices[0];
		
		if(i!=6)
			wall[3] = vertices[i+3];
		else
			wall[3] = vertices[1];

					
		return wall;
		
	}
	private float[] getShadow(int[] vertices, float f,float g){
		float shadow[] = new float[8];
		
		
		
		//Bottom Left
		shadow[0] = vertices[0];
		shadow[1] = vertices[1];

		//Bottom Right
		shadow[2] = vertices[2];
		shadow[3] = vertices[3];
		
		int xDist = (int) (vertices[0]-f);
		int yDist = (int) (vertices[1]-g);
		
		int xDist2 = (int) (vertices[2]-f);
		int yDist2 = (int) (vertices[3]-g);

		//top Left
		shadow[6] = vertices[0]+xDist*20;
		shadow[7] = vertices[1]+yDist*20;
		
		//top Right
		shadow[4] = vertices[2]+xDist2*20;
		shadow[5] = vertices[3]+yDist2*20;
		
		for(int i = 0; i < 8; i+=2)
			shadow[i] -= (f-300);
		
		for(int i = 1; i < 8; i+=2)
			shadow[i] -= (g-300);
		
		
		return shadow;
	}
	
	public Rectangle getBox(){
		return box;
	}
	
	public Polygon[] getShadows(float f, float g){
		return makeShadow(f, g);
	}
	
	public double getX(){
		return(xPos+size/2);
	}
	
	public double getY(){
		return(yPos+size/2);
	}
	

	
	public static void clearSquares(){
		squares = new ArrayList<Square>();
	}
	
	public static boolean addSquare(Square square){
		squares.add(square);
		return true;
	
	}
	
	public static void drawSquares(Graphics g){
		g.setDrawMode(Graphics.MODE_NORMAL);
		Iterator<Square> itr = squares.iterator();
		itr = squares.iterator();
		g.setColor(new Color(80,80,80));
		while(itr.hasNext()){

			Square element = itr.next();
			
			g.fill(element.getBox());
		}

	}
	
	public static List<Square> getSquares(){
		return squares;
	}

	public static List<Square> createLevelSquares(String levelName) {
		 try{

			  FileInputStream fstream = new FileInputStream(GameEngine.getLevel().pathName+"/squareData.txt");

			  DataInputStream in = new DataInputStream(fstream);
			  BufferedReader br = new BufferedReader(new InputStreamReader(in));
			  String strLine;
			  //Read File Line By Line
			  int i = 0;
			  int xPos = 0, yPos = 0, width = 0, height;
			  
			  while ((strLine = br.readLine()) != null){
				if(strLine.contains("Square")){
				  	  i=0;
				}else{
					//System.out.println(strLine);
					i++;
					int tempNum = Integer.parseInt(strLine);
					switch(i){
						case 1:	xPos = tempNum;
								break;
						case 2: yPos = tempNum;
								break;
						case 3: width = tempNum;
								break;
						case 4: height = tempNum;
								Square newSquare = new Square(xPos, yPos, width, height);
								squares.add(newSquare);
								break;
					}
					}
				}
				
			  System.out.println("Added "+squares.size()+" walls.");
	
			  in.close();
			    }catch (Exception e){
			    	System.err.println("Error reading square data.");
			    	System.err.println("Error: " + e.getMessage());
			  }
		 return squares;
		
	}
}
