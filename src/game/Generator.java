package game;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class Generator extends Object{
	private boolean on = true;
	private Sound click;
	
	Generator(float x, float y){
		super(x,y);
		try {
			click = new Sound("sounds/switchOff.wav");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	
	public boolean getMode(){
		return on;
	}
	
	public void setMode(boolean mode){
		this.on = mode;
		if(!mode){
			Guard.findGenerator();
			click.play();
			GameEngine.setMusic(mode);
		}
		
		GameEngine.setLights(mode);
		
	}
	
//	public void draw(Graphics g){
//		g.setColor(new Color(127,127,127,255));
//		g.fillRect(this.x-5, this.y-2, 10, 5);
//		g.setColor(Color.red);
//		if(getMode())
//			g.setColor(Color.green);
//		g.fillRect(this.x-3, this.y-1, 1, 1);
//		}
}
